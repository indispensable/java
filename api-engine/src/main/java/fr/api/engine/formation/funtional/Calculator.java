package fr.api.engine.formation.funtional;

@FunctionalInterface
public interface Calculator {

    int calculate(int t1, int t2);
}
