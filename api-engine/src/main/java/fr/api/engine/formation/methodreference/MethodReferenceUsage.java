package fr.api.engine.formation.methodreference;

import fr.api.engine.formation.model.Balance;
import fr.api.engine.formation.model.BalanceType;

import java.math.BigDecimal;
import java.util.function.Supplier;

public class MethodReferenceUsage {

    public void example() {
        Supplier<String> stringSupplier = new MethodReferenceUsage()::getTest;
        stringSupplier.get();

        Supplier<String> stringSupplier1 = MethodReferenceUsage::getStaticMethod;
        stringSupplier1.get();
    }

    public void getBalance() {
        Balance balance = new Balance(BigDecimal.ONE, BalanceType.AVAILABLE);
    }

    private static String getStaticMethod() {
        return "simple static method";
    }

    private String getTest() {
        return "method reference.";
    }

}
