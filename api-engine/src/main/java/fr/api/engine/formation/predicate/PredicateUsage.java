package fr.api.engine.formation.predicate;


import fr.api.engine.formation.model.Balance;
import fr.api.engine.formation.model.BankAccount;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static fr.api.engine.formation.model.BalanceType.AVAILABLE;
import static fr.api.engine.formation.model.BankAccount.*;

public class PredicateUsage {

    public void test() {
        Balance balance = new Balance(new BigDecimal(200), AVAILABLE);
        BankAccount bankAccount = new BankAccount("", Arrays.asList(balance));
        BankAccount account = new BankAccount("FR76", Arrays.asList(balance));

        Predicate<BankAccount> bankAccountPredicate = BankAccount::validateIban;

        List<BankAccount> bankAccounts = Arrays.asList(bankAccount);

        List<BankAccount> resulta = bankAccounts.stream().filter(bankAccountPredicate).collect(Collectors.toList());

    }
}
