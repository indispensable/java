package fr.api.engine.formation.stream;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUsage {

    public String concatListOfString(List<String> listOfString) {
        StringBuilder stringBuilder = new StringBuilder();
        listOfString.forEach(stringBuilder::append);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("");
        return stringBuilder.toString();
    }

    public String concatListOfInteger(List<Integer> list) {
        StringBuilder stringBuilder = new StringBuilder();
        list.forEach(stringBuilder::append);
        return stringBuilder.toString();
    }

    public String concatWithStream(List<Integer> numbers) {
        return numbers.stream()
                .map(n -> n.toString())
                .collect(Collectors.joining());
    }

    public String concatWithDelimiter(List<Integer> numbers) {
        return numbers.stream().map(number -> number.toString()).collect(Collectors.joining(","));
    }

    public void joint() {
        StringJoiner sj = new StringJoiner(":", "[", "]");
        sj.add("George").add("Sally").add("Fred");
        String desiredString = sj.toString();
        List<String> myList = Arrays.asList("HOTEL", "BANK");
        Stream<String> resul = myList.stream();
        Stream<List<String>> r = Stream.of(myList);
        String a = myList.stream().collect(Collectors.joining("|"));
    }
}
