package fr.api.engine.formation.model;


import java.math.BigDecimal;
import java.util.Objects;

public final class Order {
    /**
     * Number of ticket in format : 20140203
     */
    private final String number;

    private final BigDecimal amount;

    public Order(String number, BigDecimal amount) {
        Objects.requireNonNull(number);
        Objects.requireNonNull(amount);
        this.number = number;
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getNumber() {
        return number;
    }
}
