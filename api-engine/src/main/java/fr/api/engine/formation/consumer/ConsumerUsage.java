package fr.api.engine.formation.consumer;


import java.util.function.Consumer;

public class ConsumerUsage {

    public void example() {
        Consumer<String> stringConsumer = s -> System.out.println(s);
        Consumer<String> stringConsumer1 = System.out::println;
    }
}
