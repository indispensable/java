package fr.api.engine.formation.model;


import java.math.BigDecimal;

public final class Balance {

    private final BigDecimal amount;

    private final BalanceType type;

    public Balance(BigDecimal amount, BalanceType type) {
        this.amount = amount;
        this.type = type;
    }

    public BalanceType getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
