package fr.api.engine.formation.stream;

import fr.api.engine.formation.model.Person;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamsExamples {

    public void example() {
        List<Person> persons = Arrays.asList(new Person("AMARA", 31));
        Stream<Person> personStream = persons.stream();
        personStream.forEach(person -> System.out.print(person));
        personStream.forEach(System.out::print);
    }
}
