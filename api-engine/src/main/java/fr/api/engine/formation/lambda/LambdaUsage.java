package fr.api.engine.formation.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class LambdaUsage {

    private String check;

    public String processWithoutLambda() throws InterruptedException {
        Thread thread = new Thread( new java.lang.Runnable() {
            @Override
            public void run() {
                check = "i am running";
            }
        } );
        thread.start();
        return check;
    }

    public String processUsingLambda() {
        Thread thread = new Thread( () -> check = "i am running" );
        thread.start();
        return check;
    }

    public void runnable() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println( "alive ---1" );
            }
        };
        runnable.run();
        Runnable runnableWithLambda = () -> System.out.println( "alive ---2" );
        runnableWithLambda.run();
    }

    public void compare() {
        //http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/Lambda-QuickStart/index.html
        List<String> list = Arrays.asList( "azul", "amara" );
        list.forEach( System.out::print );// print azul et amara
        Stream.of( list ).forEach( System.out::print );// ici on print le stream alors un seul element [azul, amara]
    }
}
