package fr.api.engine.formation.model;


import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.function.Predicate;

public final class BankAccount {

    private final String iban;

    private final List<Balance> balances;

    public BankAccount(String iban, List<Balance> balances) {
        this.iban = iban;
        this.balances = balances;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public String getIban() {
        return iban;
    }

    public static Predicate<BankAccount> validate() {
        return bankAccount -> validateIban(bankAccount.getIban());
    }

    public static boolean validateIban(String iban) {
        return StringUtils.isBlank(iban);
    }

    public static boolean validateIban(BankAccount bankAccount) {
        return false;
    }
}
