package fr.api.engine.formation.supplier;

import org.junit.Test;

import java.util.Date;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;

public class SupplierUsageTest {

    @Test
    public void should_return_system_date() {
        Supplier<String> stringSupplier = () -> "hello";
        String result = stringSupplier.get();
        Supplier<Date> dateSupplier = SupplierUsage::getSystemDate;
        Date date = dateSupplier.get();
        assertThat(date).isNotNull();
    }

}