package fr.api.engine.formation.funtional;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;


public class CalculatorTest {

    @Test
    public void should_return_sum() {
        // One
        Calculator sum = (t1, t2) -> t1 + t2;
        int result = sum.calculate(1, 2);
        assertThat(result).isEqualTo(3);
        // Two
        List<Integer> list = Arrays.asList(1, 2);
        IntStream intStream = list.stream().mapToInt(Integer::intValue);
        //IntStream intStream = list.stream().mapToInt(value -> value);
        int sumResult = intStream.sum();
        assertThat(sumResult).isEqualTo(3);
        // Three
        Calculator sum2 = Integer::sum;
        int result_2 = sum2.calculate(1, 2);
        assertThat(result_2).isEqualTo(3);
    }

    @Test
    public void should_return_subtraction() {
        Calculator subtraction = (t1, t2) -> t1 - t2;
        int result = subtraction.calculate(2, 1);
        assertThat(result).isEqualTo(1);
    }

    @Test
    public void example_function() {
        Function<Integer, Integer> multiple = integer -> integer * integer;
        Function<Integer, Integer> sum = integer -> integer + integer;
        Function<Integer, Integer> function = sum.andThen(multiple);
        int result = function.apply(2);
        assertThat(result).isEqualTo(16);
    }
}