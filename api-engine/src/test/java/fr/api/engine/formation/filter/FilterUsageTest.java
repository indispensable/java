package fr.api.engine.formation.filter;


import fr.api.engine.formation.model.Order;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterUsageTest {

    @Test
    public void should_return_order_of_may_2014() {
        // Given
        List<Order> orders = given_orders();
        // When
        List<Order> result = orders.stream().filter(order -> order.getNumber().startsWith("201405")).collect(Collectors.toList());
        // then
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void should_return_amounts_for_order_may_2014() {
        // Given
        List<Order> orders = given_orders();
        // When
        List<BigDecimal> result = orders.stream().filter(order -> order.getNumber().startsWith("201405")).map(Order::getAmount).collect(Collectors.toList());
        // then
        assertThat(result.size()).isEqualTo(1);
    }

    @Test
    public void should_return_sorted_list_ascending_order() {
        // Given
        List<Order> orders = given_orders();
        // When
        List<BigDecimal> result = orders.stream().filter(order -> order.getNumber().startsWith("201404")).map(Order::getAmount).sorted().collect(Collectors.toList());
        // then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).isEqualTo(Arrays.asList(BigDecimal.TEN, new BigDecimal(20)));
    }

    @Test
    public <T> void should_return_sorted_list_descending_order() {
        // Given
        Order order_1 = given_order("20140503", BigDecimal.ONE);
        Order order_2 = given_order("20140402", new BigDecimal(-40));
        Order order_3 = given_order("20140403", new BigDecimal(-20));
        List<Order> orders = Arrays.asList(order_1, order_2, order_3);
        // When
        List<BigDecimal> result = orders.stream().filter(order -> order.getNumber().startsWith("201404"))
                .map(Order::getAmount)
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        // Then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).isEqualTo(Arrays.asList(new BigDecimal(-20), new BigDecimal(-40)));
    }

    private List<Order> given_orders() {
        Order order_1 = given_order("20140503", BigDecimal.ONE);
        Order order_2 = given_order("20140402", BigDecimal.TEN);
        Order order_3 = given_order("20140403", new BigDecimal(20));
        return Arrays.asList(order_1, order_2, order_3);
    }

    private Order given_order(String number, BigDecimal amount) {
        return new Order(number, amount);
    }
}
