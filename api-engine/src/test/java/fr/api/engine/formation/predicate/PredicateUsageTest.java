package fr.api.engine.formation.predicate;

import fr.api.engine.formation.model.Balance;
import fr.api.engine.formation.model.BankAccount;
import fr.api.engine.formation.model.Person;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static fr.api.engine.formation.model.BalanceType.AVAILABLE;
import static org.assertj.core.api.Assertions.assertThat;

public class PredicateUsageTest {

    @Test
    public void test() {
        Predicate<Person> personPredicate = person -> person.getAge() > 0;
        Person person = new Person("amara", 30);
        boolean result = personPredicate.test(person);
        assertThat(result).isTrue();
    }

    @Test
    public void should_validate_bank_account() {
        Balance balance = new Balance(new BigDecimal(200), AVAILABLE);
        BankAccount bankAccount = new BankAccount("", Arrays.asList(balance));
        Predicate<BankAccount> result = BankAccount.validate();
        boolean ibanValidation = result.test(bankAccount);
        assertThat(ibanValidation).isTrue();
    }

    @Test
    public void should_validate_a_list() {
        Balance balance = new Balance(new BigDecimal(200), AVAILABLE);
        BankAccount bankAccount = new BankAccount("", Arrays.asList(balance));
        BankAccount account = new BankAccount("FR76", Arrays.asList(balance));
        List<BankAccount> bankAccounts = Arrays.asList(bankAccount, account);
        boolean resultsss = bankAccounts.stream().allMatch(BankAccount.validate());
        Predicate<BankAccount> result = BankAccount.validate();
        boolean ibanValidation = result.test(bankAccount);
        assertThat(ibanValidation).isTrue();
    }

    @Test
    public void valide(){
        Balance balance = new Balance(new BigDecimal(200), AVAILABLE);
        BankAccount bankAccount = new BankAccount("", Arrays.asList(balance));
        BankAccount account = new BankAccount("FR76", Arrays.asList(balance));
        Predicate<BankAccount> bankAccountPredicate = BankAccount::validateIban;
        List<BankAccount> bankAccounts = Arrays.asList(bankAccount);
        List<BankAccount> resulta = bankAccounts.stream().filter(bankAccountPredicate).collect(Collectors.toList());
        assertThat(resulta).isNull();
    }
}