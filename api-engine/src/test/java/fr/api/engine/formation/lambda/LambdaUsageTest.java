package fr.api.engine.formation.lambda;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class LambdaUsageTest {

    @Test
    public void should_execute_processing_without_using_lambda_expression() throws InterruptedException {
        //Given
        LambdaUsage lambdaUsage = new LambdaUsage();
        // When
        String result = lambdaUsage.processWithoutLambda();
        // Then
        assertThat( result ).isNotBlank();
    }

    @Test
    public void should_execute_processing_using_lambda_expression() {
        // Given
        LambdaUsage lambdaUsage = new LambdaUsage();
        // When
        String result = lambdaUsage.processUsingLambda();
        // Then
        assertThat( result ).isNotBlank();

    }

    @Test
    public void should_run() {
        LambdaUsage lambdaUsage = new LambdaUsage();
        lambdaUsage.runnable();
        lambdaUsage.compare();
    }
}