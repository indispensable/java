package fr.api.engine.formation.stream;

import org.junit.Test;

import java.util.Arrays;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class StreamUsageTest {

    private StreamUsage sut = new StreamUsage();

    @Test
    public void should_return_concat_numbers_to_string_using_string_builder() {
        String result = sut.concatListOfInteger(asList(1, 2, 3));
        assertThat(result).isEqualTo("123");

    }

    @Test
    public void should_return_list_as_string() {
        String result = sut.concatListOfString(asList("a", "b", "c"));
        assertThat(result).isEqualTo("abc");
    }

    @Test
    public void should_return_concat_numbers_to_string() {
        String result = sut.concatWithStream(Arrays.asList(1, 2));
        assertThat(result).isEqualTo("12");
    }

    @Test
    public void should_return_concat_numbers_to_string_separate_by_delimiter() {
        String result = sut.concatWithDelimiter(Arrays.asList(1, 2));
        assertThat(result).isEqualTo("1,2");
    }
}