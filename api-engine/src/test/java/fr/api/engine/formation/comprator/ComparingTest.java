package fr.api.engine.formation.comprator;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ComparingTest {

    @Test
    public void test_1() {
        // Given
        List<Integer> integers = Arrays.asList(1, 2, 3);
        // When
        List<Integer> result = integers.stream().sorted(Comparator.comparingInt(Integer::byteValue).reversed()).collect(Collectors.toList());
        // then
        assertThat(result).isEqualTo(Arrays.asList(3, 2, 1));
    }

    @Test
    public void test_2() {
        // Given
        List<Integer> integers = Arrays.asList(1, 2, 3);
        // When
        List<Integer> result = integers.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
        // then
        assertThat(result).isEqualTo(Arrays.asList(3, 2, 1));
    }

    @Test
    // Float pour représenter des nombres avec des virgule, moin de précision et il est sur 32 bits
    // Double pour représenter des nombres avec virgule,  représenter sur 64 bit plus de précision.
    public void test_3() {
        System.out.println("Min value of float = " + Float.MIN_VALUE);
        System.out.println("Max value of float = " + Float.MAX_VALUE);
        System.out.println("Max length of float = " + Float.SIZE);
        System.out.println("Min value of double = " + Double.MIN_VALUE);
        System.out.println("Max value of double = " + Double.MAX_VALUE);
        System.out.println("Max length of double = " + Double.SIZE);
        float exFloat = 3.2f;
        double exDouble = 3.2;
        if (exFloat == 3.2) {
            System.out.println("exFloat equals to 3.2");
        } else {
            System.out.println("exFloat doesn't equal to 3.2");
        }
        if (exDouble == 3.2) {
            System.out.println("exDouble equals to 3.2");
        } else {
            System.out.println("exDouble doesn't equal to 3.2");
            System.out.println("exDouble's value is " + exDouble);
        }
        System.out.printf("Float value : %.20f\n", 3.2f);
        System.out.printf("Double value : %.20f\n", 3.2);
    }
}
