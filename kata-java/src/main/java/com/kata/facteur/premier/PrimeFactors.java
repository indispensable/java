package com.kata.facteur.premier;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactors {

    public List<Integer> findPrimeFactors( int inputVal ) {
        List<Integer> factors = new ArrayList<Integer>();
        for ( int i = 2; i <= inputVal; i++ ) {
            boolean isPremier = isPrimeNumber( i );
            if ( isPremier ) {
                if ( inputVal % i == 0 ) {
                    factors.add( i );
                    inputVal = inputVal / i;
                }
            }
        }
        return factors;
    }

    public boolean isPrimeNumber( int input ) {
        boolean isPrimeNumber = true;
        for ( int i = 2; i < input; i++ ) {
            if ( input % i == 0 ) {
                isPrimeNumber = false;
            }
        }
        return isPrimeNumber;
    }

    // second method to find factors of elements.
    public static List<Integer> findFactors( int n ) {
        ArrayList<Integer> factors = new ArrayList<Integer>();
        int candidate = 2;
        while ( n > 1 ) {
            if ( n % candidate == 0 ) {
                factors.add( candidate );
                n = n / candidate;
            } else {
                candidate++;
            }
        }
        return factors;
    }
}
