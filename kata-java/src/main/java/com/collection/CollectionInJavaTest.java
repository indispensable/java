package com.collection;

import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CollectionInJavaTest {

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Test
    public void should_return_array_list() {
        List<String> list = CollectionInJava.buildArrayList();
        Assert.assertEquals( false, list.isEmpty() );
        Assert.assertEquals( 4, list.size() );
    }

    @Test
    public void should_return_Exception() throws Exception {
        List<String> list = CollectionInJava.buildArrayList();
        expected.expect( Exception.class );
        CollectionInJava.delete( list );
    }

    @Test
    public void should_update_element() throws Exception {
        // given
        String countryToUpdate = "FRANCE";
        String countryTosave = "KABYLIE";
        // then
        List<String> actual = CollectionInJava.updateElement( CollectionInJava.buildArrayList(), countryToUpdate, countryTosave );
        // when
        Assert.assertTrue( actual.contains( "KABYLIE" ) );
        Assert.assertFalse( actual.contains( "FRANCE" ) );
    }

    @Test
    public void should_update_() throws Exception {
        // given
        String countryToUpdate = "KABYLIE";
        String countryTosave = "KABYLIE";
        // then
        expected.expect( Exception.class );
        CollectionInJava.updateElement( CollectionInJava.buildArrayList(), countryToUpdate, countryTosave );
    }

    @Test
    public void sould_return_tried_list() {
        // given
        List<String> orderList = CollectionInJava.buildArrayList();
        // when
        List<String> actual = CollectionInJava.orderList( orderList );
        // then
        Assert.assertEquals( "ALGERIE", actual.get( 0 ) );
    }
}
