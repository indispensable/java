package com.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class CollectionInJava {

    private static final Logger LOGGER = Logger.getLogger( CollectionInJava.class.getName() );

    public static List<String> buildArrayList() {
        List<String> listCountry = new ArrayList<>();
        listCountry.add( "FRANCE" );
        listCountry.add( "ALGERIE" );
        listCountry.add( "USA" );
        listCountry.add( "ESPAGNE" );
        LOGGER.info( "la taille de la collection est : " + listCountry.size() );
        for ( String country : listCountry ) {
            System.out.println( country.toString() );
        }
        return listCountry;
    }

    public static List<String> delete( List<String> list ) throws Exception {
        list.removeAll( list );
        if ( list.isEmpty() ) {
            throw new Exception( "liste vide." );
        }
        return list;
    }

    public static List<String> updateElement( List<String> buildArrayList, String country, String countryTosave ) throws Exception {
        int index = buildArrayList.indexOf( country );
        if ( index >= 0 ) {
            buildArrayList.remove( index );
            buildArrayList.add( countryTosave );
        } else {
            throw new Exception( "element n'existe pas " );
        }
        return buildArrayList;
    }

    public static List<String> orderList( List<String> orderList ) {
        Collections.sort( orderList );
        return orderList;
    }
}
