package com.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

//@formatter:off

/**
        \d      Un chiffre, �quivalent � : [0-9]
        \D      Un non-chiffre : [^0-9]
        \s      Un caract�re blanc : [ \t\n\x0B\f\r]
        \S      Un non-caract�re blanc : [^\s]
        \w      Un caract�re de mot : [a-zA-Z_0-9]
        \W      Un caract�re de non-mot : [^\w]
        .       Tout caract�re
        
        
        --
        
        \b matches the empty string at the beginning or end of a word.
        
 */
//@formatter:on

// https://openclassrooms.com/courses/concevez-votre-site-web-avec-php-et-mysql/les-expressions-regulieres-partie-1-2

public class RegrexTest {

    @Test
    public void case_one() {
        // given
        // when
        boolean actual = Regrex.checkRegex( "a", "[abc]" );
        // then
        Assert.assertTrue( actual );
    }

    @Test
    public void case_two() {
        // given
        // when
        boolean actual = Regrex.checkRegex( "a", "[a-z0-9]" );
        // then
        Assert.assertTrue( actual );
    }

    @Test
    public void case_thre() {
        // given
        // when
        boolean actual = Regrex.checkRegex( "A", "([A-Z]*)" );
        // then
        Assert.assertTrue( actual );
    }

    @Test
    public void case_four() {
        // given
        // when
        // ACTUAL : TRUE
        boolean actual = Regrex.checkRegex( "A", "([A-Z]{1})" );// MAX UNE
                                                                // LETTRE MAJ

        // result : FAUX
        boolean result = Regrex.checkRegex( "AAA", "([A-Z]{2})" );// MAX 2
                                                                  // LETTRE MAJ
        // then
        Assert.assertTrue( actual );
        Assert.assertFalse( result );
    }

    @Test
    public void case_five() {

        //@formatter:off
        
        /**
         * 
          - UNE CHAINE MODELE [] CORRESPAND A UN SEUL CARACTERE DE LA CHAINE A TESTER.
          
          - () EX : (w) => LA STRING DOIT AVOIR JUSTE UN SEUL CHARACTERE QUI EST = w : 
                          
                          EX :  boolean result = Pattern.matches("(w)", "w");
                                Assert.assertEquals( true, result ); 
                                
                                boolean result = Pattern.matches( "(w)", "ww" );
                                Assert.assertEquals( false, result ); 
                                
                                boolean result = Pattern.matches( "(w)", "A" );
                                Assert.assertEquals( false, result );
                                
            - POUR AVOIR : www.facebook.fr
            
            LA REGLE  : (www)(.)[a-z]+(.)[a-z]{2}
            
            EXEMPLE : 
             
            boolean actual = Regrex.checkRegex( "www.f.fr", "(www)(.)[a-z](.)[a-z]{2}" );
            Assert.assertEquals( true, actual );  
               
            boolean actual = Regrex.checkRegex( "www.facebook.fr", "(www)(.)[a-z](.)[a-z]{2}" );
                   
                   FAUX CAR www. CORRESPAND  A (www)(.)
                      --> facebook NE CORRESPAND PAS A :  [a-z]
                      
            Assert.assertEquals( false, actual );
             
            boolean result = Pattern.matches( "[a-z]", "w" );
            Assert.assertEquals( true, result );
             
         */
        
        //@formatter:on

        boolean actual = Regrex.checkRegex( "www.facebook.fr", "(www)(.)[a-z](.)[a-z]{2}" );
        Assert.assertEquals( false, actual );

        boolean result = Pattern.matches( "[a-z]", "w" );
        Assert.assertEquals( true, result );

    }

    @Test
    public void findString() {
        boolean actual = Pattern.matches( "[a-z]*", "aatafaa" );
        Assert.assertTrue( actual );
        Assert.assertTrue( find( "[a-z]*", "aaaa" ) );
        Assert.assertTrue( find( "[a-zA-Z_0-9]", "a" ) );
        Assert.assertEquals( true, find( "(www)(.)[a-z](.)[a-z]{2}", "www.facebook.fr" ) );
        Assert.assertEquals( true, find( "[a-zA-Z_0-9]", "a9" ) );

    }

    private boolean find( String regex, String input ) {
        // ATTENTION :
        //@formatter:off
                /*
                        Raccourci : 

                        Si l'on souhaite simplement v�rifier une chaine par rapport � un motif, on peut simplement utiliser la m�thode de classe matches de la classe Pattern 
                        qui prend en param�tre le motif et la cible ou alors la m�thode matches de la classe String qui prend un param�tre le motif.
                        
                        boolean m1 = Pattern.matches ("\\d+", "B737");        // false
                        boolean m2 = "A380".matches ("\\d+");                    // false
                        
                        listing js2.7 M�thode matches des classes Pattern et String
                        La m�thode matches va tenter de faire correspondre toute la chaine avec le motif. L'avantage de cette technique est la concision du code et peut-�tre utilis� si vous faites quelques comparaison. 
                        Par contre, si vous devez r�utiliser le m�me motif avec plusieurs chaines, il sera plus efficace de constuire un seul objet Pattern et de l'utiliser pour toutes les chaines � interroger.
                 */
        //@formatter:on
        Pattern pattern = Pattern.compile( regex );
        Matcher matcher = pattern.matcher( input );
        return matcher.find();
    }

    @Test
    public void test_() {

        boolean actual = Pattern.matches( "[a-zA-Z_0-9]*", "a9" );
        Assert.assertEquals( true, actual );

        boolean result = Pattern.matches( ".\\b", "7" );
        Assert.assertEquals( true, result );

    }
}
