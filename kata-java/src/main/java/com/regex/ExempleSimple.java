package com.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExempleSimple {

    public static void main( String[] args ) {
        Pattern pattern = Pattern.compile( "[A-Z]{3}-\\d{3}" );

        Matcher matcher = pattern.matcher( "KDI-927" ); // true
        System.out.println( matcher.matches() );

        matcher = pattern.matcher( "Mon immatriculation est : KDI-927" ); // false
        System.out.println( "dd" + matcher.matches() );
    }
}
