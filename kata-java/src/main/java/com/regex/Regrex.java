package com.regex;

import java.util.regex.Pattern;

/**
 * 
 * 
 * ATTTENTION
 * 
 * EXEMPLE :
 * 
 * REGEX = "\\bcat\\b"; // cherche le mot "cat" dans toute la chaine
 * 
 * String INPUT = "cat cat";
 * 
 * -- > PREND LE INPUT POUR UN SEUL CARACTERE.
 * 
 * Pattern.matches( REGEX, INPUT );// input = cat cat
 * 
 * -- > INPUT SERA PARCCOURU
 * 
 * PREMIER INPUT = cat
 * 
 * DEUXIEME INPUT SERA = cat
 * 
 * Pattern pattern = Pattern.compile( REGEX );
 * 
 * Matcher matcher = pattern.matcher( INPUT );
 * 
 */
public class Regrex {

    public static boolean checkRegex( String input, String regex ) {
        return Pattern.matches( regex, input );
    }
}
