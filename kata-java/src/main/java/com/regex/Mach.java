package com.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mach {
    private static final String REGEX = "\\bcat\\b";
    private static final String INPUT = "cat cat";

    public static void main( String[] args ) {

        boolean result = Pattern.matches( REGEX, INPUT ); // prend input comme
                                                          // un seul caract�re.
        System.out.println( result );// false

        Pattern pattern = Pattern.compile( REGEX );
        Matcher matcher = pattern.matcher( INPUT );

        // Attention une fois que j'ai fait un matcher.find() alors on passe a
        // l'element suivant de la chaine oki...

        // System.out.println( "Le r�sultat : " + matcher.find() );

        int count = 0;
        while ( matcher.find() ) {
            count++;
            System.out.println( "Match number : " + count );
        }
    }

}
