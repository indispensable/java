package com.kata.facteur.premier;

import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

public class PrimeFactorsTest {

    @Test
    public void should_return_one_and_five() {
        // given
        PrimeFactors primeFactors = new PrimeFactors();
        int input = 5;
        // when
        List<Integer> actual = primeFactors.findPrimeFactors( input );
        // then
        Assertions.assertThat( actual ).contains( 5 );
    }

    @Test
    public void should_return_three_two() {
        // given
        PrimeFactors primeFactors = new PrimeFactors();
        int input = 8;
        // when
        List<Integer> actual = primeFactors.findPrimeFactors( input );
        // then
        Assertions.assertThat( actual ).contains( 2, 2, 2 );
    }

    @Test
    public void should_return_prime_number() {
        // given
        PrimeFactors primeFactors = new PrimeFactors();
        int input = 13;
        // when
        boolean actual = primeFactors.isPrimeNumber( input );
        // then
        Assertions.assertThat( actual ).isTrue();
    }

    @Test
    public void should_return_prime_number_19() {
        // given
        PrimeFactors primeFactors = new PrimeFactors();
        int input = 19;
        // when
        boolean actual = primeFactors.isPrimeNumber( input );
        // then
        Assertions.assertThat( actual ).isTrue();
    }

    @Test
    public void should_return_list_with_6_elet() {
        // given
        PrimeFactors primeFactors = new PrimeFactors();
        int input = 100;
        // when
        List<Integer> actual = primeFactors.findPrimeFactors( input );
        // then
        Assertions.assertThat( actual ).contains( 2, 2, 5, 5 );
    }

    @Test
    public void sould_return_one_element_in_list() {
        Assert.assertEquals( Arrays.asList( 2 ), PrimeFactors.findFactors( 2 ) );
    }

    @Test
    public void sould_return_three_and_three() {
        PrimeFactors primeFactors = new PrimeFactors();
        Assert.assertEquals( Arrays.asList( 2, 5 ), primeFactors.findPrimeFactors( 10 ) );
    }

}
