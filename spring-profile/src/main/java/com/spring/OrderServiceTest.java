package com.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = { "classpath:app-config.xml" } )
@ActiveProfiles( "prod" )
public class OrderServiceTest {

    @Autowired
    private ServiceForTest serviceForTest;

    @Test
    public void testOrderService() {
        System.out.println( "---------begin test --------" );
        serviceForTest.findClient();
    }
}