package com.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceForTest {

    @Autowired
    private ClientService clientService;

    public void findClient() {
        System.out.println( "-------ServiceForTest-------" );
        clientService.find();
    }
}
