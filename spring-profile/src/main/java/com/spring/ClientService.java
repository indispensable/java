package com.spring;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile( "prod" )
public class ClientService {
    public void find() {
        System.out.println( "------- ClientService -------" );
    }
}
