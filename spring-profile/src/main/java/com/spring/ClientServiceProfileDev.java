package com.spring;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile( "dev" )
public class ClientServiceProfileDev {

    public ClientServiceProfileDev() {
        System.out.println( "-------------profile activ� est bien dev -----------" );
    }
}
