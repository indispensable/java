package com.collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * A List is a ordered collection that can contain duplicate values. It provides
 * three general purpose implementations.
 *
 */
public class ListInterface {

    public static void main( String[] args ) {

        /**
         * ArrayList is said to be the best performing list implementation in
         * normal conditions.
         */
        List<String> arrayList = new ArrayList<>();
        arrayList.add( "TOTO" );
        arrayList.add( "TATA" );
        arrayList.add( null );
        /**
         * LinkedList : liste cha�n�e est un peu plus lent que ArrayList mais il
         * fonctionne mieux dans certaines conditions.
         */
        List<String> linkedList = new LinkedList<>();
        linkedList.add( "TOTO" );
        linkedList.add( "TATA" );
        linkedList.add( null );
        /**
         * Vector is also a growable array of objects, mais contrairement �
         * ArrayList Vector est thread-safe dans la nature.
         */
        List<String> vectors = new Vector<>();
        vectors.add( "TOTO" );
        vectors.add( "TATA" );
    }
}
