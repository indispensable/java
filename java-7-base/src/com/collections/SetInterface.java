package com.collections;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * A set is a Interface that does not contain duplicate values. In provides
 * three general purpose implementations in Java.
 *
 */
public class SetInterface {

    public static void main( String[] args ) {
        /**
         * HashSet is the best performing implementation of Set interface. It
         * stores its elements in a HashTable an does not guarantee of any type
         * of ordering in iteration.
         */
        Set<String> hashSetList = new HashSet<>();
        hashSetList.add( "TOTO" );
        hashSetList.add( "TOTO" );
        System.out.println( hashSetList );

        /**
         * TreeSet : TreeSet est un peu lent que HashSet et il stocke ses
         * �l�ments dans une structure arborescente. TreeSet ordonne ses
         * �l�ments sur la base de leurs valeurs.
         */
        Set<String> treeSetList = new TreeSet<>();
        treeSetList.add( "TOTO" );
        treeSetList.add( "TATA" );
        treeSetList.add( "TOTO" );
        System.out.println( treeSetList );

        /**
         * LinkedHashSet : est impl�ment� comme une table de hachage avec une
         * liste cha�n�e. Il ordonne � ses �l�ments sur la base de l'ordre dans
         * lequel ils ont �t� ins�r�s.
         */
        Set<String> linkedHashSetList = new LinkedHashSet<>();
        linkedHashSetList.add( "TOTO" );
        linkedHashSetList.add( "TOTO" );
        System.out.println( "azul " + linkedHashSetList );
        boolean value = linkedHashSetList.remove( "TOTO" );
        System.out.println( value );

    }
}
