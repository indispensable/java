package com.collections;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Map : collection sous la forme d'une association de paires cl�/valeur. la cl�
 * est unique mais La valeur, au contraire, peut �tre associ�e � plusieurs cl�s.
 * 
 * Ces objets ont comme point faible majeur leur rapport conflictuel avec la
 * taille des donn�es � stocker. En effet, plus vous aurez de valeurs � mettre
 * dans un objet Map, plus celles-ci seront lentes et lourdes : logique, puisque
 * par rapport aux autres collections, il stocke une donn�e suppl�mentaire par
 * enregistrement. Une donn�e c'est de la m�moire en plus.
 */
public class MapInterface {

    public static void main( String[] args ) {

        /**
         * HashMap : c'est iml�mentation de l'interface Map. Permet d'associer
         * des donn�e sous forme cles valeur, utilis� dans une application � un
         * processus elle est plus rapide qu'une Hashtable.
         * 
         */
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put( "clef one", "toto" );

        /**
         * Hashtable : c'est une iml�mentation de l'interface Map. Permet
         * d'associer des donn�e sous forme cles valeur. C'est une classe
         * synchronis�. ie sera utilis� dans des applications Multithread. Mais
         * attention elle est moin performantes dans les applications normal �
         * un processus. Elle n'accepte pas la valeur null pour les clef ou pour
         * les valeurs.
         */
        Hashtable<Integer, String> ht = new Hashtable<Integer, String>();
        ht.put( 1, "printemps" );
        ht.put( 10, "�t�" );
        ht.put( 12, "automne" );
        ht.put( 45, "hiver" );
        Enumeration<String> e = ht.elements();

        while ( e.hasMoreElements() )
            System.out.println( e.nextElement() );

        /**
         * TreeMap : Impl�mentation d'interface Map. pas doublons. Associ� des
         * donn�es cle/valeur ces valeurs serons ordonn�es selon les cl�s. (et
         * on peut bien ajouter un comparator.
         * 
         */
        Map<String, String> treeMap = new TreeMap<>();
        treeMap.put( "TOTO", "azul" );
        treeMap.put( "TOTO", "azul" );
        treeMap.put( "AA", "SALUT" );
        System.out.println( treeMap );

    }

    // Hashtable vs HashMap

}
