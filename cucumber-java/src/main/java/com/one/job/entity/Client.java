package com.one.job.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang.builder.EqualsBuilder;

@Entity( name = "CLIENT" )
public class Client {

    @Id
    @Column( name = "ID" )
    private final String name;

    @Column( name = "NAME" )
    private final String id;

    @Override
    public int hashCode() {
        return Objects.hash( id );
    }

    @Override
    public boolean equals( Object obj ) {
        if ( obj == null ) {
            return false;
        }
        if ( obj == this ) {
            return true;
        }
        if ( obj.getClass() != getClass() ) {
            return false;
        }
        Client rhs = (Client) obj;
        return new EqualsBuilder().append( id, rhs.id )
                .append( id, rhs.id ).isEquals();
    }

    public static Client createClient( String name, String id ) {
        return new Client( name, id );
    }

    private Client() {
        name = null;
        id = null;
    }

    private Client( String name, String id ) {
        this.name = name;
        this.id = id;
    }

    public String changeStatusToSuspend( String expected ) {
        System.out.println( "je suis la azulllllllll" );
        return "suspend";
    }
}
