package com.one.job.configuration;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.one.job.configuration.JPAConfiguration;
import com.one.job.entity.Client;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = JPAConfiguration.class )
@Transactional
public class JpaConfigIT {
    /**
     * I will test all the mapping of different entity.
     */
    @PersistenceContext
    EntityManager systemUmbdedTest;

    @Test
    public void should_return_client() {

        // given
        Client expected = Client.createClient( "amara", "1" );
        systemUmbdedTest.persist( expected );

        // when
        Client client = systemUmbdedTest.find( Client.class, "1" );

        // then
        assertThat( client ).isEqualTo( client );
    }
}