package com.one.job.entity;

import org.assertj.core.api.Assertions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ClientStep {

    private Client client;

    private String statusWhen;

    @Given( "^existing client account number 30$" )
    public void existing_client_account_number_30() {
        client = Client.createClient( "nom client", "30" );
    }

    @When( "^client status is (.*)$" )
    public void client_status_is_active( String status ) {
        statusWhen = client.changeStatusToSuspend( status );
    }

    @Then( "^client be change change status to (.*)$" )
    public void client_be_change_change_status_to_suspend( String expectedStatus ) {
        Assertions.assertThat( statusWhen ).isEqualTo( expectedStatus );
    }
}