package com.lamada;

@FunctionalInterface
public interface MathOperation {

    public int operation( int a, int b );
}
