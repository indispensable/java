package com.lamada;

public class MathCalculate {
    public static int additon( int param1, int param2 ) {
        MathOperation addition = ( a, b ) -> a + b;
        return addition.operation( param1, param2 );
    }

    public static int multiplication( int a, int b ) {
        MathOperation multiplication = ( param1, param2 ) -> param1 * param2;
        return multiplication.operation( a, b );
    }
}
