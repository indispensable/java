package com.widcard;

// BST class can only be instantiated with classes X which implement the AbstractDao interface.
public class BTS<X extends AbstractDao> {

    public void findTemplateDataBase() {
    }
}
