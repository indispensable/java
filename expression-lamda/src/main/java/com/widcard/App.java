package com.widcard;

public class App {
    public static void main( String[] args ) {

        // Every time you instantiate the Garage, the type parameter has to be a
        // subclass of Vehicle.

        Garage<Vehicule> garageVehicule = new Garage<>();
        garageVehicule.getClass();

        Garage<Vehicule> garageDeBMW = new Garage<>();
        garageDeBMW.getClass();

        // second example
        BTS<EdgeJdbc> edge = new BTS<>();
        edge.findTemplateDataBase();

        EdgeJdbc edgeJdbc = new EdgeJdbc();
        edgeJdbc.getTemplateJdbc();
    }
}
