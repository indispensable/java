package com.widcard;

import java.util.List;

public interface AbstractDao {

    public void getTemplateJdbc();

    // restrict the type parameter in the method
    // type of T is declared in the first of method.
    public <T extends Vehicule> void findClient( List<T> list );

    public <T> T findCustomer( List<T> list );

    // Type here is not important than i can make ?
    void calculateSize( List<? extends Vehicule> list );

    <T> void calculate( List<T> list );

    // Widcard
    int totalValue( List<? super Vehicule> valuer );

    // when i use Super or Extends

}
