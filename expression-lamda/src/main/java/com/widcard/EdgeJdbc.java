package com.widcard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class EdgeJdbc implements AbstractDao {

    @Override
    public void getTemplateJdbc() {

    }

    @Override
    public <T extends Vehicule> void findClient( List<T> list ) {
    }

    @Override
    public <T> T findCustomer( List<T> list ) {
        T a = list.get( 0 );
        return a;
    }

    @Override
    // le type de la liste c'est classe qui h�rite de Vehicule.
    public void calculateSize( List<? extends Vehicule> list ) {

    }

    @Override
    public <T> void calculate( List<T> list ) {

    }

    @Override
    public int totalValue( List<? super Vehicule> valuer ) {
        return 0;
    }

    public void methodTest( List<? super Vehicule> list ) {

        Vehicule e = new Vehicule( 20 );
        // i can add element because " List<? super Vehicule> " je peut savoir
        // la classe m�re de Vehicule.
        list.add( e );
    }

    // example about super bounds:
    // le type de la liste c'est une classe dont la classe m�re est Vehicule.
    public List<? super Vehicule> buildList( List<? super Vehicule> listToBuild ) {

        listToBuild.add( new Vehicule( 20 ) );

        listToBuild.add( new VoitureBMW( 20 ) ); // la classe mere de VoitureBMW
                                                 // est Vehicule

        return listToBuild;
    }

    public int totaleFuel( List<? extends Vehicule> listOfCar ) {
        // i use get then i will use : extends not super.
        // JAVA 8 stream. map to INT and use function SUM
        IntStream var = listOfCar.stream().mapToInt( vehicule -> vehicule.getFuel() );
        int total = var.sum();
        return total;
    }

    public int totalValue( Valuer<? super Vehicule> valuer ) {

        return 0;
    }

    public static void main( String[] args ) {

        EdgeJdbc edgeJdbc = new EdgeJdbc();

        List<String> listOFString = new ArrayList<>();
        listOFString.add( "toto" );
        String var = edgeJdbc.findCustomer( listOFString );
        System.out.println( var );

        List<Integer> listOFInteger = new ArrayList<>();
        listOFInteger.add( 12 );
        Integer var2 = edgeJdbc.findCustomer( listOFInteger );
        System.out.println( var2 );

        // type of parameters of method
        List<? super Vehicule> valuer = null;

        // example of parameters.
        List<Vehicule> listVehicle = new ArrayList<>();

        // other parameters.
        List<Object> listDeString = new ArrayList<>();
        listDeString.add( "azul" );

        // call method
        edgeJdbc.totalValue( listVehicle );
        edgeJdbc.totalValue( listDeString );

        // somme avec stream
        List<Vehicule> listWithCar = Arrays.asList( new Vehicule( 20 ), new Vehicule( 20 ) );
        int laSommeTotale = edgeJdbc.totaleFuel( listWithCar );
        System.out.println( "La somme totale est de : " + laSommeTotale );

        List<? super Vehicule> listToBuild = new ArrayList<>();
        // build List
        edgeJdbc.buildList( listToBuild );
    }
}
