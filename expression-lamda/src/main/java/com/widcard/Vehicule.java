package com.widcard;

public class Vehicule {

    private final int fuel;

    public Vehicule( int fuel ) {
        this.fuel = fuel;
    }

    public int getFuel() {
        return fuel;
    }
}
