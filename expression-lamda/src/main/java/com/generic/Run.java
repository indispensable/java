package com.generic;

import java.util.function.Consumer;

public class Run {

    public static int COMPTEUR = 0;

    public static void main( String[] args ) {

        Consumer<Client> consumer = ( client ) -> {

            COMPTEUR++;

            String name = client.getName().concat( "TOTO Name" );

            client = new Client( name );

            System.out.println( client.getName() );

        };

        Consumer<Client> var = new Consumer<Client>() {

            @Override
            public void accept( Client t ) {
                consumer.accept( t );
            }

            @Override
            public Consumer<Client> andThen( Consumer<? super Client> after ) {
                System.out.println( "salut" );
                return consumer.andThen( after );
            }
        };
        consumer.accept( new Client( "tafni " ) );
        var.accept( new Client( "sd" ) );
        var.andThen( var );
        System.out.println( "Compteur : " + COMPTEUR );

    }
}
