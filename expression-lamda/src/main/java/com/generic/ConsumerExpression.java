package com.generic;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExpression {

    public static void main( String[] args ) {
        List<Student> students = Arrays.asList( new Student( 1, 1, "John" ), new Student( 2, 2, "Amar" ) );

        //
        Consumer<Student> raiser = element -> {
            element.gpa = element.gpa + 1;
            element.id = element.id + 1;
        };
        // first parameters student.
        // Second execute action raiser andthen execute (System.out::println)
        raiseStudents( students, raiser.andThen( System.out::println ) );
    }

    private static void raiseStudents( List<Student> employees, Consumer<Student> fx ) {
        // for each element execute Consumer code.
        // after each action execute andThen method
        employees.stream().forEach( em -> {
            fx.accept( em );
            System.out.println( "azul : " + em.toString() );
        } );
    }
}

class Student {
    public int    id;
    public int    gpa;
    public String name;

    Student( int id, int g, String name ) {
        this.id = id;
        this.gpa = g;
        this.name = name;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + gpa;
    }
}