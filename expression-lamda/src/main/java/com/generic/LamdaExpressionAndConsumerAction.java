package com.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class LamdaExpressionAndConsumerAction {

    public static void find( List<?> list ) {

        // display list
        list.forEach( element -> System.out.println( element ) );

        // build other list of String
        List<String> listToBuild = new ArrayList<>();

        list.forEach( element -> {
            String param = element.toString().concat( " : toto" );
            listToBuild.add( param );
        } );

        // display new List with lambda expression
        listToBuild.forEach( element -> System.out.println( element ) );

        // display list with Consumer : associate action for each element.
        listToBuild.forEach( new Consumer<String>() {
            @Override
            public void accept( String element ) {
                System.out.println( element );
            }
        } );
    }

    // run
    public static void main( String[] args ) {

        List<String> listOfString = Arrays.asList( "azul flawen", "bonjour tout le monde" );
        find( listOfString );

        List<Integer> listOfInteger = Arrays.asList( 1, 2, 3 );
        find( listOfInteger );
    }
}
