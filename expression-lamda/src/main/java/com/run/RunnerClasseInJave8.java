package com.run;

interface Executable {
    public int execute( int a, int b );
}

class Runner {

    private int a;
    private int b;

    public Runner( int a, int b ) {
        this.a = a;
        this.b = b;
    }

    public void run( Executable executable ) {
        System.out.println( "Block Runner" );
        executable.execute( a, b );
    }
}

public class RunnerClasseInJave8 {

    private int result;

    public int executeAdditionOperation() {
        int a = 0;
        int b = 0;
        Runner runner = new Runner( a, b );
        runner.run( new Executable() {
            @Override
            public int execute( int a, int b ) {
                result = a + b;
                return result;
            }
        } );
        return result;
    }

    public int executeMultiplicationOperation() {
        Runner runner = new Runner( 7, 8 );
        runner.run( new Executable() {
            @Override
            public int execute( int a, int b ) {
                result = a * b;
                return result;
            }
        } );
        return result;
    }
}
