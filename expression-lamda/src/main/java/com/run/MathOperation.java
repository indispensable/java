package com.run;

interface ExecutableAction {
    public void execute();
}

class RunnerAction {
    public void run( ExecutableAction executableAction ) {
        System.out.println( "block first **********" );
        executableAction.execute();
    }
}

public class MathOperation {

    public void executeAdditionOperation() {

        RunnerAction runner = new RunnerAction();

        runner.run( new ExecutableAction() {

            @Override
            public void execute() {
                System.out.println( "block second **********" );
            }
        } );

        // LAMDA expression

        runner.run( () -> System.out.println( "azul" ) );
        int b = 0;
        int a = 1;

        runner.run( () -> {
            System.out.println( "s" );
            System.out.println( a + b );
        } );

        ExecutableAction executableAction = () -> {
            System.out.println( "" );
            System.out.println( "" );
        };

        runner.run( executableAction );

    }
}