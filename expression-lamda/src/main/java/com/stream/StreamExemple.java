package com.stream;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExemple {

    public static Set<Person> findClientByName( String name ) {

        Set<Person> givenListPerson = givenListPerson();

        // Condition for Filter by name
        Predicate<? super Person> predicate = person -> person.getName().equals( name );

        Stream<Person> persons = givenListPerson.stream().filter( predicate );

        // Transform Stream to Set
        Set<Person> collectToSetList = persons.collect( Collectors.toSet() );

        return collectToSetList;
    }

    // just to unit the List of Person
    private static Set<Person> givenListPerson() {
        Set<Person> listSet = new HashSet<>();
        listSet.add( new Person( "amara" ) );
        listSet.add( new Person( "azul" ) );
        return listSet;
    }
}
