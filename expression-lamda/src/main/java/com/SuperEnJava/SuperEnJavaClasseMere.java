package com.SuperEnJava;

import java.util.List;

import com.widcard.Vehicule;

public class SuperEnJavaClasseMere {

    // Default Constructor.
    public SuperEnJavaClasseMere() {
        // validateTransaction();
        System.out.println( "----Constructeur class mere-----" );
    }

    // method
    public String validateTransaction() {
        System.out.println( "------methode in class mere------" );
        return "validate classe mere";
    }

    public <T extends E, E> int calculate( List<T> list ) {
        return 1;
    }

    public <T> int find( List<? extends T> list ) {
        return 1;
    }

    public int test( List<? extends Vehicule> list ) {
        return 1;
    }
}
