package com.SuperEnJava;

public class SuperEnJava extends SuperEnJavaClasseMere {

    public SuperEnJava() {
        System.out.println( "-------------Constructeur de la class fille-------" );
    }

    @Override
    public String validateTransaction() {

        return super.validateTransaction();
    }

    public static void main( String[] args ) {
        // au moment de l'instanciation de la classe fille on instancier d'abord
        // la classe m�re.
        new SuperEnJava();
    }
}
