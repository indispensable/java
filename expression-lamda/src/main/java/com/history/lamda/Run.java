package com.history.lamda;

public class Run {

    public static <R, T> void main( String[] args ) {
        // le but ici j'ai appeler une methode d'une interface sans que cette
        // classe soit implements SimpleInterfaceFunctionel
        SimpleInterfaceFunctionel exempleOne = () -> System.out.println( "Premi�re implementation de la methode find." );
        exempleOne.find();

        SimpleInterfaceFunctionel exempleTwo = () -> System.out.println( "Deuxi�me implentation de la method find." );
        exempleTwo.find();
    }
}
