package com.history.lamda;

import java.util.Arrays;
import java.util.List;

public class PersonneVersionJava7 {

    private final int    age;
    private final String name;

    @SuppressWarnings( "unused" )
    private PersonneVersionJava7() {
        age = 0;
        name = null;
    }

    public PersonneVersionJava7( int age, String name ) {
        this.age = age;
        this.name = name;
    }

    public static void main( String[] args ) {
        List<PersonneVersionJava7> personnes = Arrays.asList( new PersonneVersionJava7( 25, "toto 1" ), new PersonneVersionJava7( 25, "toto 2" ) );
        int sum = 0;
        int nbPersonne = 0;
        // With JDK 1.7
        for ( PersonneVersionJava7 personne : personnes ) {
            if ( personne.getAge() > 20 ) {
                nbPersonne++;
                sum = sum + personne.getAge();
            }
        }
        if ( !personnes.isEmpty() ) {
            int average = sum / nbPersonne;
            System.out.println( "La moyenne d'age est de : " + average );
        }
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }
}
