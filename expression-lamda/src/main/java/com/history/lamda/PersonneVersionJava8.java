package com.history.lamda;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class PersonneVersionJava8 {

    private final int    age;
    private final String name;

    @SuppressWarnings( "unused" )
    private PersonneVersionJava8() {
        age = 0;
        name = null;
    }

    public PersonneVersionJava8( int age, String name ) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " " + age;
    }

    public static void main( String[] args ) {
        // with stream and lambda expression
        List<PersonneVersionJava8> personnes = Arrays.asList( new PersonneVersionJava8( 25, "toto 1" ), new PersonneVersionJava8( 25, "toto 2" ) );

        List<PersonneVersionJava8> lists = personnes.stream().filter( personne -> personne.getAge() > 20 ).collect( Collectors.toList() );

        List<Integer> ageList = lists.stream().map( personne -> personne.getAge() ).collect( Collectors.toList() );

        ageList.forEach( p -> System.out.println( "azul " + p ) );

        // faire la somme
        int sum = personnes.stream().filter( personne -> personne.getAge() > 20 ).mapToInt( personne -> personne.getAge() ).sum();
        System.out.println( "la somme : " + sum );
        OptionalDouble average = personnes.stream().filter( personne -> personne.getAge() > 30 ).mapToInt( personne -> personne.getAge() ).average();

        if ( average.isPresent() ) {
            double laMoyenne = average.getAsDouble();
            System.out.println( "la moyenne est : " + laMoyenne );
        }
        // other method
        double average2 = personnes.stream().filter( personne -> personne.getAge() > 30 ).mapToInt( personne -> personne.getAge() ).average().getAsDouble();
        System.out.println( average2 );
    }
}
