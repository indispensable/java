package com.history.lamda;

@FunctionalInterface
public interface Lambda {

    public void find();

}
