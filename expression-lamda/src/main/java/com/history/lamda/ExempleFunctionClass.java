package com.history.lamda;

@FunctionalInterface
public interface ExempleFunctionClass<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t
     *            the function argument
     * @return the function result
     */
    R process( T t );

    default void validate( T t ) {

    };
}
