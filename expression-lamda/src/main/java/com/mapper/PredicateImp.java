package com.mapper;

public class PredicateImp implements Predicate<Integer> {

    @Override
    public boolean filter( Integer o ) {
        return o >= 20;
    }

}
