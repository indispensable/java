package com.mapper;

public interface Mapper<O, R> {
    R map( O o );
}
