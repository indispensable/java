package com.mapper;

public class MapperImp implements Mapper<Client, Integer> {

    @Override
    public Integer map( Client o ) {
        return o.getAge();
    }
}
