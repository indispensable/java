package com.mapper;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class JavaSetListExample {
    public static void main( String[] args ) {
        // new comparator
        Comparator<? super Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare( Integer o1, Integer o2 ) {
                return o1.compareTo( o2 );
            }
        };
        // TreeMap must use new Comparator to tree this map
        Map<Integer, Montagne> listMap = new TreeMap<Integer, Montagne>( comparator );
        listMap.put( 4, new Montagne( 20, 0 ) );
        listMap.put( 1, new Montagne( 30, 2 ) );
        listMap.put( 3, new Montagne( 30, 9 ) );

        // putAll : Tree map
        listMap.putAll( listMap );

        // to Browse this list
        for ( Iterator<?> iterator = listMap.keySet().iterator(); iterator.hasNext(); ) {
            Montagne montagne = listMap.get( iterator.next() );
            System.out.println( montagne.getPlace() );
        }
    }
}

class Montagne implements Comparable<Montagne> {

    private final int hauteur;
    private final int place;

    public Montagne( int hauteur, int place ) {
        this.hauteur = hauteur;
        this.place = place;
    }

    public int getHauteur() {
        return hauteur;
    }

    public int getPlace() {
        return place;
    }

    @Override
    public int compareTo( Montagne o ) {
        return this.getHauteur() + o.getHauteur();
    }
}