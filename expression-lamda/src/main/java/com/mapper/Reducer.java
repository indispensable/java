package com.mapper;

public interface Reducer<R> {

    R reduce( R r1, R r2 );

}
