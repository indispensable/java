package com.mapper;

import java.util.Arrays;
import java.util.List;

public class Run {

    public static void main( String[] args ) {

        // with LAMBDA EXPRESSION we can :
        Mapper<Client, Integer> mapper = ( Client clientParam ) -> clientParam.getAge();
        int result = mapper.map( new Client( 20 ) );
        // return 20
        System.out.println( result );

        // other way to use Lambda expression :
        Mapper<Client, Integer> mapper2 = Client::getAge;
        Integer result2 = mapper2.map( new Client( 30 ) );
        // return 30
        System.out.println( result2 );

        // filter --> predicate :
        Predicate<Client> predicate = ( Client client ) -> client.getAge() >= 20;
        boolean age = predicate.filter( new Client( 30 ) );
        System.out.println( age );

        // Reduce in java 8
        Reducer<Client> reducer = ( Client client1, Client client2 ) -> {
            Client client = new Client( 30 );
            return client;
        };

        // example one
        Client client = reducer.reduce( new Client( 30 ), new Client( 40 ) );
        System.out.println( client.getAge() );

        // example two
        Reducer<Integer> reducer2 = ( Integer param1, Integer param2 ) -> param1 + param2;
        Integer resultReduce = reducer2.reduce( 20, 30 );
        System.out.println( resultReduce );

        List<Client> persons = Arrays.asList( new Client( 30 ), new Client( 40 ) );

        // List<Integer> ages = Lists.map( persons, person -> person.getAge() );
        // List<Integer> ages20 = Lists.filter( ages, age -> age >= 20 );
        // int sum = Lists.reduce( ages20, ( r1, r2 ) -> r1 + r2 );

    }
}
