package com.mapper;

public class Client {

    private final int age;

    public Client( int age ) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
