package com.mapper;

public interface Predicate<O> {

    boolean filter( O o );
}
