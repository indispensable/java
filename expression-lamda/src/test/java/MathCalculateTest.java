import org.junit.Assert;
import org.junit.Test;

import com.lamada.MathCalculate;

public class MathCalculateTest {

    @Test
    public void should_return_four_for_given_addtion_context() {

        // given
        int b = 2;
        int a = 2;
        // when
        int actual = MathCalculate.additon( a, b );
        // then
        Assert.assertEquals( 4, actual );
    }

    @Test
    public void should_return_four_for_given_multiplication_context() {

        // given
        int b = 2;
        int a = 2;
        // when
        int actual = MathCalculate.multiplication( a, b );
        // then
        Assert.assertEquals( 4, actual );
    }
}
