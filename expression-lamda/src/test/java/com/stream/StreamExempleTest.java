package com.stream;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class StreamExempleTest {

    @Test
    public void should_return_emtyList() {

        // given
        String name = "a";
        // when
        Set<Person> listOfPerson = StreamExemple.findClientByName( name );
        // then
        Assert.assertTrue( listOfPerson.isEmpty() );
    }

    @Test
    public void should_not_return_emtyList() {

        // given
        String name = "amara";
        // when
        Set<Person> listOfPerson = StreamExemple.findClientByName( name );
        // then
        Assert.assertFalse( listOfPerson.isEmpty() );
        Assert.assertEquals( 1, listOfPerson.size() );
    }
}
