import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.one.job.common.Address;
import com.one.job.configuration.JPAConfiguration;
import com.one.job.customer.Company;
import com.one.job.dao.DefaultCompanyDao;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = JPAConfiguration.class )
@Transactional
public class CompanyDaoIT {

    @PersistenceContext
    private EntityManager     entityManager;

    @Autowired
    private DefaultCompanyDao defaultCompanyDao;

    @Test
    public void should_return_company() {
        // given
        Company expected = givenCompany();

        // when
        defaultCompanyDao.addCompany( expected );
        // i will use entityManager to find result after call DAO
        Company result = entityManager.find( Company.class, expected.getId() );
        // then
        assertThat( result ).isNotNull();
    }

    private Company givenCompany() {
        return new Company( "tafni", "0652988054", "tafni.amar@gmail.com",
                new Address( "0652988054", "75000", "PARIS", "FRANCE" ) );
    }
}
