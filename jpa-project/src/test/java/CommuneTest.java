import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.one.job.common.Commune;

public class CommuneTest {
    /**
     * It�s used to test the exception throw by the method.
     */
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void should_equals_return_true() {

        // GIVEN
        Commune communeOne = givenCommune( new Long( 20 ) );
        Commune communeTwo = givenCommune( new Long( 20 ) );
        // WHEN
        boolean actual = communeOne.equals( communeTwo );
        // THEN
        assertThat( actual ).isTrue();
    }

    @Test
    public void should_equals_return_false() {

        // GIVEN
        Commune communeOne = givenCommune( new Long( 20 ) );
        Commune communeTwo = givenCommune( new Long( 21 ) );
        // WHEN
        boolean actual = communeOne.equals( communeTwo );
        // THEN
        assertThat( actual ).isFalse();
    }

    @Test
    public void should_method_equals_not_null() {

        // GIVEN
        Commune communeOne = givenCommune( new Long( 0 ) );
        thrown.expect( NumberFormatException.class );
        // thrown.expectMessage( null );
        Commune communeTwo = givenCommune( new Long( null ) );
        // WHEN
        boolean actual = communeOne.equals( communeTwo );
        // THEN
        assertThat( actual ).isFalse();
    }

    private Commune givenCommune( Long idOdCommun ) {
        return new Commune( idOdCommun, "PAEIS", "commune de paris" );
    }
}
