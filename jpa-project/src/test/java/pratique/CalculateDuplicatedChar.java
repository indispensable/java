package pratique;

public class CalculateDuplicatedChar {

    public int calculate( String inputText ) {
        int cpt = 0;
        String[] a = inputText.split( "" );
        String val2 = null;
        for ( int i = 0; i <= a.length - 1; i++ ) {
            String val1 = a[i];
            if ( i == a.length - 1 ) {
                val2 = null;
            } else {
                val2 = a[i + 1];
            }
            if ( val1.equals( val2 ) ) {
                cpt = cpt + 1;
                i = i + 1;
            }
        }
        return cpt;
    }
}
