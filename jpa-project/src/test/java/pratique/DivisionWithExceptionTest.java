package pratique;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class DivisionWithExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test(expected = ArithmeticException.class )
    public void should_throw_excpetion() {

        // GIVEN
        int a = 2;
        int b = 0;
        // WHEN
        DivisionWithException divisionWithException = new DivisionWithException();
        // thrown.expect( ArithmeticException.class );
        divisionWithException.findDivision( a, b );

    }

    @Test(expected = ArithmeticException.class )
    public void should_return_good_division() {

        // GIVEN
        DivisionWithException divisionWithException = new DivisionWithException();
        int a = 4;
        int b = 2;
        // WHEN

        divisionWithException.findDivision( a, 0 );// test will be passed and
                                                   // does not continue.

        int actual = divisionWithException.findDivision( a, b );
        // THEN
        Assertions.assertThat( actual ).isEqualTo( 8 );// test will be failed
                                                       // but it's not
    }
}
