package pratique;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CalculateDuplicatedCharTest {
    @Test
    public void should_return_two() {
        // given
        CalculateDuplicatedChar calculateDuplicatedChar = new CalculateDuplicatedChar();
        String inputString = "abbbttt";
        // when
        int actual = calculateDuplicatedChar.calculate( inputString );
        // then
        assertThat( actual ).isEqualTo( 2 );
    }

    @Test
    public void should_return_two_also() {
        // given
        CalculateDuplicatedChar calculateDuplicatedChar = new CalculateDuplicatedChar();
        String input = "aabbbtest";
        // when
        int actual = calculateDuplicatedChar.calculate( input );
        // then
        assertThat( actual ).isEqualTo( 2 );
    }

    @Test
    public void should_return_four() {
        // given
        CalculateDuplicatedChar calculateDuplicatedChar = new CalculateDuplicatedChar();
        String input = "rrrr";
        // when
        int actual = calculateDuplicatedChar.calculate( input );
        // then
        assertThat( actual ).isEqualTo( 2 );
    }

    @Test
    public void should_return_one() {
        // given
        CalculateDuplicatedChar calculateDuplicatedChar = new CalculateDuplicatedChar();
        String input = "rr";
        // when
        int actual = calculateDuplicatedChar.calculate( input );
        // then
        assertThat( actual ).isEqualTo( 1 );
    }

    @Test
    public void should_return_nine() {
        // given
        CalculateDuplicatedChar calculateDuplicatedChar = new CalculateDuplicatedChar();
        String input = "rrsssssssssssssssfdfdgffrgfg";
        // when
        int actual = calculateDuplicatedChar.calculate( input );
        // then
        assertThat( actual ).isEqualTo( 9 );
    }
}
