package pratique;

public class DivisionWithException {
    /**
     * 
     * @param a
     * @param b
     * @return result of division a/b
     */
    public int findDivision( int a, int b ) {

        return a / b;
    }
}
