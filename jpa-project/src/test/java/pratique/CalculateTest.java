package pratique;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateTest {

    public static final int stringOccur( String text, String string ) {
        return regexOccur( text, Pattern.quote( string ) );
    }

    public static final int regexOccur( String text, String regex ) {
        Matcher matcher = Pattern.compile( regex ).matcher( text );
        int occur = 0;
        while ( matcher.find() ) {
            occur++;
        }
        return occur;
    }

    public static void main( String[] args ) {
        System.out.println( "voila " + stringOccur( "a", "Salut comment �a va " ) );
    }
}
