import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.one.job.configuration.JPAConfiguration;
import com.one.job.mandate.Creditors;
import com.one.job.mandate.Mandate;
import com.one.job.mandate.MandatePK;
import com.one.job.mandate.PeriodicityMandate;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = JPAConfiguration.class )
@Transactional
public class MappingEntityIT {

    @PersistenceContext
    private EntityManager entityManager;

    @Before
    public void init() {
        Mandate mandate1 = new Mandate( new MandatePK( "123", "++123", "FR761" ), "EDF",
                new PeriodicityMandate( new MandatePK( "123", "++123", "FR761" ), new BigDecimal( 30 ), new Date() ),
                new Creditors( "FR761", "EDF" ) );
        entityManager.persist( mandate1 );
        Mandate mandate2 = new Mandate( new MandatePK( "123", "++124", "FR762" ), "orange",
                new PeriodicityMandate( new MandatePK( "123", "++124", "FR762" ), new BigDecimal( 30 ), new Date() ),
                new Creditors( "FR762", "orange" ) );
        entityManager.persist( mandate2 );
    }

    @Test
    public void should_return_company() {
        // given
        Query query = entityManager.createQuery( "from MANDATE  where ACCOUNT_NUMBER = :ACCOUNT_NUMBER", Mandate.class );
        query.setParameter( "ACCOUNT_NUMBER", "123" );
        @SuppressWarnings( "unchecked" )
        List<Mandate> listeMandate = query.getResultList();
        Assertions.assertThat( listeMandate.size() ).isEqualTo( 2 );
        Mandate result = entityManager.find( Mandate.class, givenMandatePk() );
        PeriodicityMandate periodicityMandate = result.getPeriodicityMandate();
        assertThat( periodicityMandate ).isEqualTo( new PeriodicityMandate( givenMandatePk(), new BigDecimal( 30 ), new Date() ) );
        assertThat( result.getCreditors().getCreditorName() ).isEqualTo( "orange" );
    }

    private MandatePK givenMandatePk() {
        return new MandatePK( "acount", "run", "ics" );
    }
}