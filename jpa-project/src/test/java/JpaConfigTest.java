
import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.one.job.common.Address;
import com.one.job.common.Commune;
import com.one.job.common.Maire;
import com.one.job.configuration.JPAConfiguration;
import com.one.job.customer.Company;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = JPAConfiguration.class )
@Transactional
public class JpaConfigTest {
    /**
     * I will test all the mapping of different entity.
     */
    @PersistenceContext
    EntityManager systemUmbdedTest;

    @Test
    public void should_return_company() {

        // given
        systemUmbdedTest.persist( givenCompany() );

        // when
        Company company = systemUmbdedTest.find( Company.class, givenCompany().getId() );

        // then
        assertThat( company ).isNotNull();
        assertThat( company ).isEqualTo( givenCompany() );
    }

    @Test
    public void should_return_Commune() {

        // given
        systemUmbdedTest.persist( givenCommune() );

        // when
        Commune actual = systemUmbdedTest.find( Commune.class, givenCommune().getId() );
        // then
        assertThat( actual ).isNotNull();
        assertThat( actual.getMaire() ).isEqualTo( givenMaire() );
    }

    @Test
    public void should_return_Maire() {
        // given
        Commune commune = givenCommune();
        systemUmbdedTest.persist( commune );
        // when
        Maire actual = systemUmbdedTest.find( Maire.class, commune.getMaire().getId() );
        // then
        // oneToOne @OneToOne( cascade = CascadeType.ALL ) then:
        // when i save Commune i will save also Maire :)
        assertThat( actual ).isEqualTo( givenMaire() );
    }

    private Commune givenCommune() {
        Commune commune = new Commune( new Long( 1 ), "Mr. TAFNI AMARA", "le maire de PARIS", givenMaire() );
        return commune;
    }

    private Maire givenMaire() {
        Maire maire = new Maire( new Long( 1 ), "Mr. TAFNI AMARA", "le maire de PARIS" );
        return maire;
    }

    private Company givenCompany() {
        return new Company( "tafni", "0652988054", "tafni.amar@gmail.com", new Address( "0652988054", "75000", "PARIS", "FRANCE" ) );
    }

}