
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.one.job.account.Creditor;
import com.one.job.account.CreditorPreferences;
import com.one.job.account.CreditorPreferencesPK;
import com.one.job.configuration.JPAConfiguration;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( classes = JPAConfiguration.class )
@Transactional
public class CreditorPreferencesTest {

    @PersistenceContext
    EntityManager entityManager;

    @Before
    public void init() {
        // GIVEN
        Creditor creditorOrange = givenCreditor( 1, "ORANGE" );
        Creditor creditorFree = givenCreditor( 2, "FREE" );
        Creditor creditorSFR = givenCreditor( 3, "SFR" );

        List<Creditor> creditorsListOrange = new ArrayList<>();
        List<Creditor> creditorsListFree = new ArrayList<>();
        List<Creditor> creditorsListSFR = new ArrayList<>();

        creditorsListOrange.add( creditorOrange );
        creditorsListFree.add( creditorFree );
        creditorsListSFR.add( creditorSFR );

        CreditorPreferences creditorPreferencesOrange = new CreditorPreferences( 1, "123", creditorsListOrange );
        CreditorPreferences creditorPreferencesFree = new CreditorPreferences( 2, "123", creditorsListFree );
        CreditorPreferences creditorPreferencesSFR = new CreditorPreferences( 3, "123", creditorsListSFR );

        // WHEN
        entityManager.persist( creditorOrange );
        entityManager.persist( creditorFree );
        entityManager.persist( creditorSFR );

        entityManager.persist( creditorPreferencesOrange );
        entityManager.persist( creditorPreferencesFree );
        entityManager.persist( creditorPreferencesSFR );

        CreditorPreferences creditorPreferences = entityManager.find( CreditorPreferences.class, new CreditorPreferencesPK( 1, "123" ) );
        List<Creditor> creditors = creditorPreferences.getCreditors();

        // THEN
        Assertions.assertThat( creditors ).hasSize( 1 );
    }

    @Test
    public void should_return_creditor_preferences() {
        // GIVEN
        // WHEN
        CreditorPreferences creditorPreferences = entityManager.find( CreditorPreferences.class, new CreditorPreferencesPK( 1, "123" ) );

        List<Creditor> creditors = creditorPreferences.getCreditors();
        // THEN
        Assertions.assertThat( creditors ).hasSize( 1 );
    }

    @Test
    public void should_return_all_creditors() {

        Query query = entityManager.createNativeQuery( "SELECT * FROM CREDITOR_PREFERENCES WHERE ACCOUNT_NUMBER = ? ", CreditorPreferences.class )
                .setParameter( 1, "123" );
        List<CreditorPreferences> creditorPreferencesList = query.getResultList();
        Assertions.assertThat( creditorPreferencesList.iterator().next().getCreditors() ).hasSize( 1 );
    }

    private Creditor givenCreditor( int creditorID, String name ) {
        return new Creditor( creditorID, name );
    }
}
