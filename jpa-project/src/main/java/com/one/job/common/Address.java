package com.one.job.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity( name = "ADDRESS" )
public final class Address {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long         id;

    @Column( name = ( "STREET_NUMBER" ))
    private final String streetNumber;

    @Column( name = "POSTALE_CODE" )
    private final String postaleCode;

    @Column( name = "CITY" )
    private final String city;

    @Column( name = "COUNTRY" )
    private final String country;

    @SuppressWarnings( "unused" )
    /**
     * used by JPA for mapping.
     */
    private Address() {
        streetNumber = null;
        postaleCode = null;
        city = null;
        country = null;
    }

    public Address( String streetNumber, String postaleCode, String city, String country ) {
        this.streetNumber = streetNumber;
        this.postaleCode = postaleCode;
        this.city = city;
        this.country = country;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getPostaleCode() {
        return postaleCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
