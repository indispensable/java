package com.one.job.common;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity( name = "MAIRE" )
public class Maire {

    @Id
    @Column( name = "ID" )
    private final Long   id;

    @Column( name = "NAME" )
    private final String name;

    @Column( name = "DESCRIPTION" )
    private final String description;

    @OneToOne( mappedBy = "maire", cascade = CascadeType.ALL )
    private Commune      commune;

    public Maire( Long id, String name, String description, Commune commune ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.commune = commune;
    }

    public Maire( Long id, String name, String description ) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @SuppressWarnings( "unused" )
    private Maire() {
        id = null;
        name = null;
        description = null;
        commune = null;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( ( obj != null ) && ( obj.getClass().equals( this.getClass() ) ) ) {
            if ( obj instanceof Maire ) {
                Maire maire = (Maire) obj;
                return maire.getId().equals( this.id );
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash( this.id );
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Commune getCommune() {
        return commune;
    }

}
