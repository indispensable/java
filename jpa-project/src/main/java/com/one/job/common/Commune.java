package com.one.job.common;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity( name = "COMMUNE" )
public class Commune {

    @Id
    @Column( name = "ID" )
    private final Long   id;

    @Column( name = "NAME" )
    private final String name;

    @Column( name = "DESCRIPTION" )
    private final String description;

    @OneToOne( cascade = CascadeType.ALL )
    private Maire        maire;

    public Commune( Long id, String name, String description, Maire maire ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.maire = maire;
    }

    public Commune( Long id, String name, String description ) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @SuppressWarnings( "unused" )
    /**
     * used by JPA for mapping.
     */
    private Commune() {
        id = null;
        name = null;
        description = null;
        maire = null;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( ( obj != null ) && ( obj.getClass().equals( obj.getClass() ) ) ) {
            if ( obj instanceof Commune ) {
                Commune commune = (Commune) obj;
                return commune.getId().equals( this.id );
            }
            return false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash( this.id );
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Maire getMaire() {
        return maire;
    }
}
