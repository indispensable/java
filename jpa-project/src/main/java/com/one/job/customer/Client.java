package com.one.job.customer;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.one.job.common.Address;

@Entity( name = "CLIENT" )
public final class Client {

    @Id
    @Column( name = ( "CIF" ))
    private final String    cif;

    @Column( name = ( "NAME" ))
    private final String    name;

    @Column( name = ( "PRENOM" ))
    private final String    prenom;

    @Column( name = ( "GENDER" ))
    private final String    gender;

    @Column( name = ( "BIRTH_DATE" ))
    private final LocalDate birthDate;

    @Column( name = ( "EMAIL" ))
    private final String    email;

    @OneToOne( cascade = { CascadeType.ALL } )
    private final Address   address;

    @Column( name = ( "PHONE_NUMBER" ))
    private final String    phoneNumber;

    @SuppressWarnings( "unused" )
    /**
     * used by JPA for mappng.f
     */
    private Client() {
        cif = null;
        name = null;
        prenom = null;
        gender = null;
        birthDate = null;
        email = null;
        address = null;
        phoneNumber = null;
    }

    public Client( String cif, String name, String prenom, String gender, LocalDate birthDate, String email,
            Address address, String phoneNumber ) {
        super();
        this.cif = cif;
        this.name = name;
        this.prenom = prenom;
        this.gender = gender;
        this.birthDate = birthDate;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public String getCif() {
        return cif;
    }

    public String getName() {
        return name;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getGender() {
        return gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getEmail() {
        return email;
    }

    public Address getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

}
