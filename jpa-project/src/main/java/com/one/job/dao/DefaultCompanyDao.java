package com.one.job.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.one.job.customer.Company;

@Repository
public class DefaultCompanyDao implements CompanyDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addCompany( Company company ) {
        entityManager.persist( company );
    }

    @Override
    public void updateCompany( Company company ) {

    }

    @Override
    public void deleteCompany( Company company ) {

    }

    @Override
    public void findCompany( Company company ) {

    }
}