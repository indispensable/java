package com.one.job.dao;

import com.one.job.customer.Company;

public interface CompanyDao {

	public void addCompany(Company company);

	public void updateCompany(Company company);

	public void deleteCompany(Company company);
	
	public void findCompany(Company company);
}
