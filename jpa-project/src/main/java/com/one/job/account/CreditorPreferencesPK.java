package com.one.job.account;

import java.io.Serializable;

public class CreditorPreferencesPK implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1182514704025312023L;

    private final int         creditorId;

    private final String      accountNumber;

    @SuppressWarnings( "unused" )
    private CreditorPreferencesPK() {
        accountNumber = null;
        creditorId = 0;
    }

    public CreditorPreferencesPK( int creditorId, String accountNumber ) {
        super();
        this.creditorId = creditorId;
        this.accountNumber = accountNumber;
    }

    public int getCreditorId() {
        return creditorId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}
