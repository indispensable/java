package com.one.job.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity( name = "CREDITOR" )
public class Creditor {

    @Id
    @Column( name = "ID" )
    private final int    id;

    @Column( name = "CREDITOR_NAME" )
    private final String creditorName;

    @SuppressWarnings( "unused" )
    private Creditor() {
        id = 0;
        creditorName = null;
    }

    public Creditor( int id, String creditorName ) {
        this.id = id;
        this.creditorName = creditorName;
    }

    public int getId() {
        return id;
    }

    public String getCreditorName() {
        return creditorName;
    }
}
