package com.one.job.account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity( name = "ACCOUNT" )
public class Account {

    @Id
    @Column( name = "ACCOUNT_NUMBER" )
    private final int accountNumber;

    @SuppressWarnings( "unused" )
    private Account() {
        accountNumber = 0;
    }

    public Account( int accountNumber ) {
        this.accountNumber = accountNumber;
    }

    public int getAccountNumber() {
        return accountNumber;
    }
}
