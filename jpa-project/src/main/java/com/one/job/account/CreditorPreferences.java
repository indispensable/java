package com.one.job.account;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;

@Entity( name = "CREDITOR_PREFERENCES" )
@IdClass( CreditorPreferencesPK.class )
public class CreditorPreferences {

    @Id
    @Column( name = "CREDITOR_ID" )
    private final int            creditorId;

    @Id
    @Column( name = "ACCOUNT_NUMBER" )
    private final String         accountNumber;

    @OneToMany
    private final List<Creditor> creditors;

    public CreditorPreferences( int creditorId, String accountNumber, List<Creditor> creditors ) {
        this.creditorId = creditorId;
        this.accountNumber = accountNumber;
        this.creditors = creditors;
    }

    @SuppressWarnings( "unused" )
    private CreditorPreferences() {
        creditorId = 0;
        accountNumber = null;
        creditors = null;
    }

    public List<Creditor> getCreditors() {
        return creditors;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getCreditorId() {
        return creditorId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( accountNumber == null ) ? 0 : accountNumber.hashCode() );
        result = prime * result + creditorId;
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        CreditorPreferences other = (CreditorPreferences) obj;
        if ( accountNumber == null ) {
            if ( other.accountNumber != null )
                return false;
        } else if ( !accountNumber.equals( other.accountNumber ) )
            return false;
        if ( creditorId != other.creditorId )
            return false;
        return true;
    }

}
