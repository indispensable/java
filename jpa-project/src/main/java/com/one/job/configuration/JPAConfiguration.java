package com.one.job.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan( JPAConfiguration.PACKAGE_TO_SCAN )
@ImportResource( locations = "/applicationContextAop.xml" )
public class JPAConfiguration {

    static final String         PACKAGE_TO_SCAN = "com.one.job";
    private static final Logger LOGGER          = LoggerFactory.getLogger( JPAConfiguration.class );

    /**
     * This is used to setup the database. It will load the schema.sql file
     * which does a create table so we have a table to work with in the project
     */
    @Bean
    public DataSourceInitializer dataSourceInitializer( DataSource dataSource ) {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
        resourceDatabasePopulator.addScript( new ClassPathResource( "/schema.sql" ) );
        DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
        dataSourceInitializer.setDataSource( dataSource );
        dataSourceInitializer.setDatabasePopulator( resourceDatabasePopulator );
        LOGGER.info( "********************** INIT DATA SOURCE *************" );
        return dataSourceInitializer;
    }

    /**
     * This will be setting up a datasource using HyperSQL (hsqldb) in memory
     */
    @Bean
    public DataSource hsqlDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName( org.hsqldb.jdbcDriver.class.getName() );
        basicDataSource.setUsername( "sa" );
        basicDataSource.setPassword( "" );
        basicDataSource.setUrl( "jdbc:hsqldb:mem:mydb" );
        return basicDataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean transactionManagerFactory( DataSource dataSource ) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource( dataSource );
        entityManagerFactoryBean.setPackagesToScan( "com.one.job" );
        entityManagerFactoryBean.setJpaVendorAdapter( new HibernateJpaVendorAdapter() );
        entityManagerFactoryBean.setPersistenceProvider( new HibernatePersistenceProvider() );
        entityManagerFactoryBean.setJpaProperties( buildJpaProperties() );
        return entityManagerFactoryBean;
    }

    private Properties buildJpaProperties() {
        Properties properties = new Properties();
        properties.put( "hibernate.dialect", "org.hibernate.dialect.H2Dialect" );
        properties.put( "hibernate.show_sql", "true" );
        properties.put( "hibernate.hbm2ddl.auto", "create-drop" );
        properties.put( "hibernate.hbm2ddl.import_files", "schema.sql" );
        properties.put( "toplink.logging.level", "FINE" );
        properties.put( "toplink.logging.timestamp", "true" );
        return properties;
    }

    @Bean
    public PlatformTransactionManager transactionManager( EntityManagerFactory entityManagerFactory ) {
        return new JpaTransactionManager( entityManagerFactory );
    }
}