package com.one.job.mandate;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity( name = "MANDATE" )
public class Mandate {

    @EmbeddedId
    private final MandatePK          mandatePK;

    @OneToOne( cascade = CascadeType.ALL )
    private final PeriodicityMandate periodicityMandate;

    @Column( name = "CREDITOR_NAME" )
    private final String             nameCreditor;

    @OneToOne( cascade = CascadeType.ALL )
    private final Creditors          creditors;

    public Mandate( MandatePK mandatePK, String nameCreditor, PeriodicityMandate periodicityMandate, Creditors creditors ) {
        this.mandatePK = mandatePK;
        this.nameCreditor = nameCreditor;
        this.periodicityMandate = periodicityMandate;
        this.creditors = creditors;
    }

    @SuppressWarnings( "unused" )
    private Mandate() {
        mandatePK = null;
        nameCreditor = null;
        periodicityMandate = null;
        creditors = null;
    }

    @Override
    public int hashCode() {
        return Objects.hash( mandatePK );
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Mandate other = (Mandate) obj;
        if ( mandatePK == null ) {
            if ( other.mandatePK != null )
                return false;
        } else if ( !mandatePK.equals( other.mandatePK ) )
            return false;
        return true;
    }

    public String getNameCreditor() {
        return nameCreditor;
    }

    public PeriodicityMandate getPeriodicityMandate() {
        return periodicityMandate;
    }

    public Creditors getCreditors() {
        return creditors;
    }
}
