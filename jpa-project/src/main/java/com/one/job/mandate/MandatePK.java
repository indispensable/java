package com.one.job.mandate;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MandatePK implements Serializable {

    private static final long serialVersionUID = -3644455091751347220L;

    @Column( name = "ACCOUNT_NUMBER" )
    private final String      accountNumber;

    @Column( name = "RUM" )
    private final String      rum;

    @Column( name = "ICS" )
    private final String      ics;

    public MandatePK( String accountNumber, String rum, String ics ) {
        super();
        this.accountNumber = accountNumber;
        this.rum = rum;
        this.ics = ics;
    }

    /**
     * 
     */
    @SuppressWarnings( "unused" )
    private MandatePK() {
        accountNumber = null;
        ics = null;
        rum = null;
    }

    @Override
    public int hashCode() {
        return Objects.hash( accountNumber, ics, rum );
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        MandatePK other = (MandatePK) obj;
        if ( accountNumber == null ) {
            if ( other.accountNumber != null )
                return false;
        } else if ( !accountNumber.equals( other.accountNumber ) )
            return false;
        if ( ics == null ) {
            if ( other.ics != null )
                return false;
        } else if ( !ics.equals( other.ics ) )
            return false;
        if ( rum == null ) {
            if ( other.rum != null )
                return false;
        } else if ( !rum.equals( other.rum ) )
            return false;
        return true;
    }

}
