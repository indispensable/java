package com.one.job.mandate;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity( name = "CREDITORS" )
public class Creditors {

    @Id
    @Column( name = "ICS" )
    private final String ics;

    @Column( name = "CREDITOR_NAME" )
    private final String creditorName;

    public Creditors( String ics, String creditorName ) {
        this.ics = ics;
        this.creditorName = creditorName;
    }

    @SuppressWarnings( "unused" )
    private Creditors() {
        ics = null;
        creditorName = null;
    }

    @Override
    public int hashCode() {
        return Objects.hash( ics );
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        Creditors other = (Creditors) obj;
        if ( ics == null ) {
            if ( other.ics != null )
                return false;
        } else if ( !ics.equals( other.ics ) )
            return false;
        return true;
    }

    public String getCreditorName() {
        return creditorName;
    }

}
