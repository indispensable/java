package com.one.job.mandate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity( name = "PERIODICITY_MANDATE" )
public class PeriodicityMandate {

    @EmbeddedId
    private final MandatePK  mandatePK;

    @Column( name = "AMOUNT" )
    private final BigDecimal amount;

    private final Date       lastUpdateDate;

    @SuppressWarnings( "unused" )
    private PeriodicityMandate() {
        mandatePK = null;
        amount = null;
        lastUpdateDate = null;
    }

    public PeriodicityMandate( MandatePK mandatePK, BigDecimal amount, Date lastUpdateDate ) {
        this.mandatePK = mandatePK;
        this.amount = amount;
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash( mandatePK );
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        PeriodicityMandate other = (PeriodicityMandate) obj;
        if ( amount == null ) {
            if ( other.amount != null )
                return false;
        } else if ( !amount.equals( other.amount ) )
            return false;
        if ( mandatePK == null ) {
            if ( other.mandatePK != null )
                return false;
        } else if ( !mandatePK.equals( other.mandatePK ) )
            return false;
        return true;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

}
